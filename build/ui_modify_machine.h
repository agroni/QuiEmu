/********************************************************************************
** Form generated from reading UI file 'modify_machine.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MODIFY_MACHINE_H
#define UI_MODIFY_MACHINE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QToolButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_modify_machine
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *general_tab;
    QLabel *label;
    QLineEdit *machine_name_txt;
    QLabel *label_2;
    QComboBox *system_type_comboBox;
    QLabel *label_7;
    QComboBox *machineType_comboBox;
    QWidget *cpu_tab;
    QLabel *label_8;
    QComboBox *cpu_model_comboBox;
    QWidget *ram_tab;
    QLineEdit *ram_size_txt;
    QLabel *label_3;
    QLabel *label_4;
    QSlider *ram_slider;
    QWidget *graphics_tab;
    QCheckBox *enable_graphics_checkbox;
    QLabel *graphics_card_label;
    QCheckBox *enable_spice_checkbox;
    QLabel *spice_port_label;
    QLineEdit *spice_port_txt;
    QComboBox *graphics_card_comboBox;
    QWidget *network_tab;
    QCheckBox *enable_if1_checkBox;
    QComboBox *if1_comboBox;
    QLabel *if1Label;
    QCheckBox *enable_samba_share_checkBox;
    QLineEdit *samba_share_path;
    QLabel *smb_path_label;
    QCheckBox *enable_if2_checkBox;
    QComboBox *if2_comboBox;
    QLabel *if2Label;
    QCheckBox *enable_if3_checkBox;
    QComboBox *if3_comboBox;
    QLabel *if3Label;
    QCheckBox *enable_user_if_passthrough_checkBox;
    QCheckBox *enable_Networking_checkBox;
    QWidget *cd_image_tab;
    QLineEdit *cd_image_path_txt;
    QLabel *choose_cd_image_label;
    QToolButton *choose_cdrom_btn;
    QCheckBox *enable_cd_checkbox;
    QWidget *disk_image_tab;
    QGroupBox *groupBox;
    QRadioButton *create_new_diskimage_radio;
    QRadioButton *use_existing_image_radio;
    QToolButton *image_choose_btn;
    QLineEdit *image_path_txt;
    QPushButton *finish_next_btn;
    QLabel *label_5;
    QWidget *tab;
    QCheckBox *enable_kvm_checkbox;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *exit_btn;
    QPushButton *apply_btn;

    void setupUi(QWidget *modify_machine)
    {
        if (modify_machine->objectName().isEmpty())
            modify_machine->setObjectName(QStringLiteral("modify_machine"));
        modify_machine->resize(800, 485);
        gridLayoutWidget = new QWidget(modify_machine);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(0, 0, 821, 451));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        gridLayout->setContentsMargins(0, 0, 0, 0);
        tabWidget = new QTabWidget(gridLayoutWidget);
        tabWidget->setObjectName(QStringLiteral("tabWidget"));
        general_tab = new QWidget();
        general_tab->setObjectName(QStringLiteral("general_tab"));
        label = new QLabel(general_tab);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(150, 130, 121, 21));
        QFont font;
        font.setPointSize(12);
        label->setFont(font);
        machine_name_txt = new QLineEdit(general_tab);
        machine_name_txt->setObjectName(QStringLiteral("machine_name_txt"));
        machine_name_txt->setGeometry(QRect(290, 130, 351, 22));
        label_2 = new QLabel(general_tab);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(150, 200, 111, 21));
        label_2->setFont(font);
        system_type_comboBox = new QComboBox(general_tab);
        system_type_comboBox->setObjectName(QStringLiteral("system_type_comboBox"));
        system_type_comboBox->setGeometry(QRect(290, 200, 221, 22));
        label_7 = new QLabel(general_tab);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(150, 270, 111, 18));
        machineType_comboBox = new QComboBox(general_tab);
        machineType_comboBox->setObjectName(QStringLiteral("machineType_comboBox"));
        machineType_comboBox->setGeometry(QRect(290, 260, 281, 24));
        tabWidget->addTab(general_tab, QString());
        cpu_tab = new QWidget();
        cpu_tab->setObjectName(QStringLiteral("cpu_tab"));
        label_8 = new QLabel(cpu_tab);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(100, 180, 161, 18));
        cpu_model_comboBox = new QComboBox(cpu_tab);
        cpu_model_comboBox->setObjectName(QStringLiteral("cpu_model_comboBox"));
        cpu_model_comboBox->setGeometry(QRect(270, 180, 391, 24));
        tabWidget->addTab(cpu_tab, QString());
        ram_tab = new QWidget();
        ram_tab->setObjectName(QStringLiteral("ram_tab"));
        ram_size_txt = new QLineEdit(ram_tab);
        ram_size_txt->setObjectName(QStringLiteral("ram_size_txt"));
        ram_size_txt->setGeometry(QRect(530, 220, 113, 22));
        QFont font1;
        font1.setFamily(QStringLiteral("Open Sans"));
        ram_size_txt->setFont(font1);
        label_3 = new QLabel(ram_tab);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(340, 110, 91, 31));
        QFont font2;
        font2.setFamily(QStringLiteral("Open Sans"));
        font2.setPointSize(14);
        label_3->setFont(font2);
        label_4 = new QLabel(ram_tab);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(660, 220, 21, 16));
        ram_slider = new QSlider(ram_tab);
        ram_slider->setObjectName(QStringLiteral("ram_slider"));
        ram_slider->setGeometry(QRect(90, 220, 421, 16));
        ram_slider->setMaximum(64000);
        ram_slider->setSingleStep(4000);
        ram_slider->setPageStep(4000);
        ram_slider->setOrientation(Qt::Horizontal);
        ram_slider->setInvertedAppearance(false);
        ram_slider->setInvertedControls(false);
        ram_slider->setTickPosition(QSlider::TicksBelow);
        ram_slider->setTickInterval(2000);
        tabWidget->addTab(ram_tab, QString());
        graphics_tab = new QWidget();
        graphics_tab->setObjectName(QStringLiteral("graphics_tab"));
        enable_graphics_checkbox = new QCheckBox(graphics_tab);
        enable_graphics_checkbox->setObjectName(QStringLiteral("enable_graphics_checkbox"));
        enable_graphics_checkbox->setGeometry(QRect(60, 155, 161, 23));
        graphics_card_label = new QLabel(graphics_tab);
        graphics_card_label->setObjectName(QStringLiteral("graphics_card_label"));
        graphics_card_label->setGeometry(QRect(60, 215, 111, 18));
        enable_spice_checkbox = new QCheckBox(graphics_tab);
        enable_spice_checkbox->setObjectName(QStringLiteral("enable_spice_checkbox"));
        enable_spice_checkbox->setGeometry(QRect(480, 155, 221, 23));
        spice_port_label = new QLabel(graphics_tab);
        spice_port_label->setObjectName(QStringLiteral("spice_port_label"));
        spice_port_label->setGeometry(QRect(480, 215, 91, 18));
        spice_port_txt = new QLineEdit(graphics_tab);
        spice_port_txt->setObjectName(QStringLiteral("spice_port_txt"));
        spice_port_txt->setGeometry(QRect(570, 210, 113, 24));
        graphics_card_comboBox = new QComboBox(graphics_tab);
        graphics_card_comboBox->setObjectName(QStringLiteral("graphics_card_comboBox"));
        graphics_card_comboBox->setGeometry(QRect(190, 215, 191, 24));
        tabWidget->addTab(graphics_tab, QString());
        network_tab = new QWidget();
        network_tab->setObjectName(QStringLiteral("network_tab"));
        enable_if1_checkBox = new QCheckBox(network_tab);
        enable_if1_checkBox->setObjectName(QStringLiteral("enable_if1_checkBox"));
        enable_if1_checkBox->setGeometry(QRect(190, 50, 241, 23));
        if1_comboBox = new QComboBox(network_tab);
        if1_comboBox->setObjectName(QStringLiteral("if1_comboBox"));
        if1_comboBox->setGeometry(QRect(190, 100, 351, 24));
        if1Label = new QLabel(network_tab);
        if1Label->setObjectName(QStringLiteral("if1Label"));
        if1Label->setGeometry(QRect(20, 100, 161, 18));
        enable_samba_share_checkBox = new QCheckBox(network_tab);
        enable_samba_share_checkBox->setObjectName(QStringLiteral("enable_samba_share_checkBox"));
        enable_samba_share_checkBox->setGeometry(QRect(10, 380, 181, 23));
        samba_share_path = new QLineEdit(network_tab);
        samba_share_path->setObjectName(QStringLiteral("samba_share_path"));
        samba_share_path->setGeometry(QRect(200, 380, 431, 24));
        smb_path_label = new QLabel(network_tab);
        smb_path_label->setObjectName(QStringLiteral("smb_path_label"));
        smb_path_label->setGeometry(QRect(200, 350, 381, 18));
        enable_if2_checkBox = new QCheckBox(network_tab);
        enable_if2_checkBox->setObjectName(QStringLiteral("enable_if2_checkBox"));
        enable_if2_checkBox->setGeometry(QRect(190, 140, 241, 23));
        if2_comboBox = new QComboBox(network_tab);
        if2_comboBox->setObjectName(QStringLiteral("if2_comboBox"));
        if2_comboBox->setGeometry(QRect(190, 190, 351, 24));
        if2Label = new QLabel(network_tab);
        if2Label->setObjectName(QStringLiteral("if2Label"));
        if2Label->setGeometry(QRect(20, 190, 161, 18));
        enable_if3_checkBox = new QCheckBox(network_tab);
        enable_if3_checkBox->setObjectName(QStringLiteral("enable_if3_checkBox"));
        enable_if3_checkBox->setGeometry(QRect(190, 240, 241, 23));
        if3_comboBox = new QComboBox(network_tab);
        if3_comboBox->setObjectName(QStringLiteral("if3_comboBox"));
        if3_comboBox->setGeometry(QRect(190, 290, 351, 24));
        if3Label = new QLabel(network_tab);
        if3Label->setObjectName(QStringLiteral("if3Label"));
        if3Label->setGeometry(QRect(20, 290, 161, 18));
        enable_user_if_passthrough_checkBox = new QCheckBox(network_tab);
        enable_user_if_passthrough_checkBox->setObjectName(QStringLiteral("enable_user_if_passthrough_checkBox"));
        enable_user_if_passthrough_checkBox->setGeometry(QRect(490, 50, 301, 23));
        enable_Networking_checkBox = new QCheckBox(network_tab);
        enable_Networking_checkBox->setObjectName(QStringLiteral("enable_Networking_checkBox"));
        enable_Networking_checkBox->setGeometry(QRect(190, 10, 181, 23));
        tabWidget->addTab(network_tab, QString());
        cd_image_tab = new QWidget();
        cd_image_tab->setObjectName(QStringLiteral("cd_image_tab"));
        cd_image_path_txt = new QLineEdit(cd_image_tab);
        cd_image_path_txt->setObjectName(QStringLiteral("cd_image_path_txt"));
        cd_image_path_txt->setGeometry(QRect(120, 230, 471, 22));
        choose_cd_image_label = new QLabel(cd_image_tab);
        choose_cd_image_label->setObjectName(QStringLiteral("choose_cd_image_label"));
        choose_cd_image_label->setGeometry(QRect(260, 150, 211, 51));
        choose_cd_image_label->setFont(font);
        choose_cdrom_btn = new QToolButton(cd_image_tab);
        choose_cdrom_btn->setObjectName(QStringLiteral("choose_cdrom_btn"));
        choose_cdrom_btn->setGeometry(QRect(590, 230, 31, 21));
        enable_cd_checkbox = new QCheckBox(cd_image_tab);
        enable_cd_checkbox->setObjectName(QStringLiteral("enable_cd_checkbox"));
        enable_cd_checkbox->setGeometry(QRect(250, 90, 221, 23));
        tabWidget->addTab(cd_image_tab, QString());
        disk_image_tab = new QWidget();
        disk_image_tab->setObjectName(QStringLiteral("disk_image_tab"));
        groupBox = new QGroupBox(disk_image_tab);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(170, 100, 411, 251));
        create_new_diskimage_radio = new QRadioButton(groupBox);
        create_new_diskimage_radio->setObjectName(QStringLiteral("create_new_diskimage_radio"));
        create_new_diskimage_radio->setGeometry(QRect(90, 70, 201, 20));
        create_new_diskimage_radio->setChecked(true);
        use_existing_image_radio = new QRadioButton(groupBox);
        use_existing_image_radio->setObjectName(QStringLiteral("use_existing_image_radio"));
        use_existing_image_radio->setGeometry(QRect(90, 120, 171, 20));
        image_choose_btn = new QToolButton(groupBox);
        image_choose_btn->setObjectName(QStringLiteral("image_choose_btn"));
        image_choose_btn->setEnabled(false);
        image_choose_btn->setGeometry(QRect(320, 160, 28, 22));
        image_path_txt = new QLineEdit(groupBox);
        image_path_txt->setObjectName(QStringLiteral("image_path_txt"));
        image_path_txt->setEnabled(false);
        image_path_txt->setGeometry(QRect(90, 160, 231, 22));
        finish_next_btn = new QPushButton(groupBox);
        finish_next_btn->setObjectName(QStringLiteral("finish_next_btn"));
        finish_next_btn->setGeometry(QRect(310, 210, 81, 22));
        label_5 = new QLabel(disk_image_tab);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(170, 50, 441, 41));
        label_5->setFont(font2);
        tabWidget->addTab(disk_image_tab, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        enable_kvm_checkbox = new QCheckBox(tab);
        enable_kvm_checkbox->setObjectName(QStringLiteral("enable_kvm_checkbox"));
        enable_kvm_checkbox->setGeometry(QRect(120, 50, 111, 23));
        tabWidget->addTab(tab, QString());

        gridLayout->addWidget(tabWidget, 1, 0, 1, 1);

        horizontalLayoutWidget = new QWidget(modify_machine);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(0, 450, 811, 79));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setSpacing(200);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(50, 0, 50, 45);
        exit_btn = new QPushButton(horizontalLayoutWidget);
        exit_btn->setObjectName(QStringLiteral("exit_btn"));

        horizontalLayout->addWidget(exit_btn);

        apply_btn = new QPushButton(horizontalLayoutWidget);
        apply_btn->setObjectName(QStringLiteral("apply_btn"));

        horizontalLayout->addWidget(apply_btn);


        retranslateUi(modify_machine);

        tabWidget->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(modify_machine);
    } // setupUi

    void retranslateUi(QWidget *modify_machine)
    {
        modify_machine->setWindowTitle(QApplication::translate("modify_machine", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("modify_machine", "Machine Name", Q_NULLPTR));
        label_2->setText(QApplication::translate("modify_machine", "System Type", Q_NULLPTR));
        system_type_comboBox->clear();
        system_type_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "Linux", Q_NULLPTR)
         << QApplication::translate("modify_machine", "BSD", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Mac OSX", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Solaris", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Windows", Q_NULLPTR)
        );
        label_7->setText(QApplication::translate("modify_machine", "Machine Type", Q_NULLPTR));
        machineType_comboBox->clear();
        machineType_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "Standard PC (Q35 + ICH9, 2009)", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Standard PC (i440FX + PIIX, 1996)", Q_NULLPTR)
        );
        tabWidget->setTabText(tabWidget->indexOf(general_tab), QApplication::translate("modify_machine", "General", Q_NULLPTR));
        label_8->setText(QApplication::translate("modify_machine", "Emulate CPU Model", Q_NULLPTR));
        cpu_model_comboBox->clear();
        cpu_model_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "Bypass Host CPU (needs KVM)", Q_NULLPTR)
         << QApplication::translate("modify_machine", "AMD Opteron 62xx (Gen 4 Class Opteron) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "AMD Opteron 23xx (Gen 3 Class Opteron) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "AMD Opteron 22xx (Gen 2 Class Opteron) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "AMD Opteron 240 (Gen 1 Class Opteron) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", " Intel Xeon E312xx (Sandy Bridge) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Intel Core i7 9xx (Nehalem Class Core i7) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Intel Core 2 Duo P9xxx (Penryn Class Core 2) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Intel Celeron_4x0 (Conroe/Merom Class Core 2) ", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Westmere E56xx/L56xx/X56xx (Nehalem-C) ", Q_NULLPTR)
        );
        tabWidget->setTabText(tabWidget->indexOf(cpu_tab), QApplication::translate("modify_machine", "CPU", Q_NULLPTR));
        label_3->setText(QApplication::translate("modify_machine", "RAM Size", Q_NULLPTR));
        label_4->setText(QApplication::translate("modify_machine", "MB", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(ram_tab), QApplication::translate("modify_machine", "RAM", Q_NULLPTR));
        enable_graphics_checkbox->setText(QApplication::translate("modify_machine", "Enable Graphics", Q_NULLPTR));
        graphics_card_label->setText(QApplication::translate("modify_machine", "Graphics Card", Q_NULLPTR));
        enable_spice_checkbox->setText(QApplication::translate("modify_machine", "Enable the SPICE Protocol", Q_NULLPTR));
        spice_port_label->setText(QApplication::translate("modify_machine", "SPICE Port", Q_NULLPTR));
        graphics_card_comboBox->clear();
        graphics_card_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "Standard", Q_NULLPTR)
         << QApplication::translate("modify_machine", "Cirrus", Q_NULLPTR)
         << QApplication::translate("modify_machine", "VMware SVGA-II", Q_NULLPTR)
         << QApplication::translate("modify_machine", "QXL", Q_NULLPTR)
        );
        tabWidget->setTabText(tabWidget->indexOf(graphics_tab), QApplication::translate("modify_machine", "Graphics", Q_NULLPTR));
        enable_if1_checkBox->setText(QApplication::translate("modify_machine", "Enable Network Interface 1", Q_NULLPTR));
        if1_comboBox->clear();
        if1_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "e1000 (Intel Gigabit Ethernet)  1Gb/s", Q_NULLPTR)
         << QApplication::translate("modify_machine", "rtl8139 (Realtek Fast Ethernet)  10/100Mb/s", Q_NULLPTR)
         << QApplication::translate("modify_machine", "virtio (Virtual High Performance Ethernet card) 1Gb/s", Q_NULLPTR)
        );
        if1Label->setText(QApplication::translate("modify_machine", "Network Interface 1", Q_NULLPTR));
        enable_samba_share_checkBox->setText(QApplication::translate("modify_machine", "Enable Samba Share", Q_NULLPTR));
        smb_path_label->setText(QApplication::translate("modify_machine", "Samba Share Path (accesible via \\\\10.0.2.4\\qemu)", Q_NULLPTR));
        enable_if2_checkBox->setText(QApplication::translate("modify_machine", "Enable Network Interface 2", Q_NULLPTR));
        if2_comboBox->clear();
        if2_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "e1000 (Intel Gigabit Ethernet)  1Gb/s", Q_NULLPTR)
         << QApplication::translate("modify_machine", "rtl8139 (Realtek Fast Ethernet)  10/100Mb/s", Q_NULLPTR)
         << QApplication::translate("modify_machine", "virtio (Virtual High Performance Ethernet card) 1Gb/s", Q_NULLPTR)
        );
        if2Label->setText(QApplication::translate("modify_machine", "Network Interface 2", Q_NULLPTR));
        enable_if3_checkBox->setText(QApplication::translate("modify_machine", "Enable Network Interface 3", Q_NULLPTR));
        if3_comboBox->clear();
        if3_comboBox->insertItems(0, QStringList()
         << QApplication::translate("modify_machine", "e1000 (Intel Gigabit Ethernet)  1Gb/s", Q_NULLPTR)
         << QApplication::translate("modify_machine", "rtl8139 (Realtek Fast Ethernet)  10/100Mb/s", Q_NULLPTR)
         << QApplication::translate("modify_machine", "virtio (Virtual High Performance Ethernet card) 1Gb/s", Q_NULLPTR)
        );
        if3Label->setText(QApplication::translate("modify_machine", "Network Interface 3", Q_NULLPTR));
        enable_user_if_passthrough_checkBox->setText(QApplication::translate("modify_machine", "Host Network Interface Passthrough", Q_NULLPTR));
        enable_Networking_checkBox->setText(QApplication::translate("modify_machine", "Enable Networking", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(network_tab), QApplication::translate("modify_machine", "Network", Q_NULLPTR));
        choose_cd_image_label->setText(QApplication::translate("modify_machine", "Choose a CD-ROM Image", Q_NULLPTR));
        choose_cdrom_btn->setText(QApplication::translate("modify_machine", "...", Q_NULLPTR));
        enable_cd_checkbox->setText(QApplication::translate("modify_machine", "Enable CD-ROM Emulation", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(cd_image_tab), QApplication::translate("modify_machine", "CD Image", Q_NULLPTR));
        groupBox->setTitle(QString());
        create_new_diskimage_radio->setText(QApplication::translate("modify_machine", "Create new Disk Image", Q_NULLPTR));
        use_existing_image_radio->setText(QApplication::translate("modify_machine", "Use an existing one", Q_NULLPTR));
        image_choose_btn->setText(QApplication::translate("modify_machine", "...", Q_NULLPTR));
        finish_next_btn->setText(QApplication::translate("modify_machine", "Next", Q_NULLPTR));
        label_5->setText(QApplication::translate("modify_machine", "Create new disk image or use an existing one", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(disk_image_tab), QApplication::translate("modify_machine", "Disk Image", Q_NULLPTR));
        enable_kvm_checkbox->setText(QApplication::translate("modify_machine", "Enable KVM", Q_NULLPTR));
        tabWidget->setTabText(tabWidget->indexOf(tab), QApplication::translate("modify_machine", "Acceleration", Q_NULLPTR));
        exit_btn->setText(QApplication::translate("modify_machine", "Exit", Q_NULLPTR));
        apply_btn->setText(QApplication::translate("modify_machine", "Apply Changes", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class modify_machine: public Ui_modify_machine {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MODIFY_MACHINE_H
