#include "src/mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent),
    ui(new Ui::MainWindow) {
        ui->setupUi(this);
        qApp->installEventFilter(this);
        QAction *removeAction = new QAction("Remove",ui->machine_listView);
        QAction *modifyAction = new QAction("Modify",ui->machine_listView);
        ui->machine_listView->addAction(removeAction);
        ui->machine_listView->addAction(modifyAction);
        ui->machine_listView->setContextMenuPolicy(Qt::ActionsContextMenu);
        connect(removeAction,SIGNAL(triggered(bool)),SLOT(on_remove_machine_btn_clicked()));
        connect(modifyAction,SIGNAL(triggered(bool)),SLOT(on_modify_machine_btn_clicked()));
        loadMachines();
        ui->machine_listView->setCurrentIndex(model->index(0,0));
        ui->machine_listView->clicked(model->index(0,0));
}

MainWindow::~MainWindow() {
    delete ui;
}

void MainWindow::loadMachines() {
    ui->machine_listView->setSelectionMode(QAbstractItemView::ExtendedSelection);
    model = new QStringListModel(this);
    machineView = new QListView;
    QDir machinesPath(QString::fromUtf8(homedir) + "/.config/QuiEmu/");
    const QString folderPath = machinesPath.filePath("Machines/");

    if(!QDir(folderPath).exists()) {
      QDir().mkdir(QString::fromUtf8(homedir) + "/.config/QuiEmu/");
      QDir().mkdir(folderPath);
    }

    if(!folderPath.isEmpty()) {
        QDir dir(folderPath);
        QStringList filter;
        filter << QLatin1String("*.json");
        dir.setNameFilters(filter);
        QFileInfoList filelistinfo = dir.entryInfoList();
        foreach (const QFileInfo &fileinfo, filelistinfo) {
            machines << fileinfo.fileName().remove(".json");
        }
        model->setStringList(machines);
        ui->machine_listView->setModel(model);
    }
    ui->machine_listView->update();
    ui->machine_listView->repaint();
}

void about() {
    QMessageBox* AboutWindow = new QMessageBox();
    AboutWindow->setWindowTitle("About QuiEmu");
    QFont font;
    font.setFamily("Open Sans");
    font.setPointSize(10);
    AboutWindow->setFont(font);
    AboutWindow->setText("QuiEmu was brought to you by Pascal Peinecke <a href=\"http://www.github.com/Pascal3366\">www.github.com/Pascal3366</a><br>This is free As in freedom software and you are allowed to copy, fork and contribute!");
    AboutWindow->setWindowFlags(Qt::WindowStaysOnTopHint);
    AboutWindow->show();
}

void MainWindow::on_actionAbout_QuiEmu_triggered() {
    about();
}

void MainWindow::on_actionDiskimage_triggered() {
    myCreate_diskimageWindow = new create_diskimage();
    myCreate_diskimageWindow->show();
}

void MainWindow::on_actionMachine_triggered() {
    this->close();
    myCreate_machineWindow = new create_machine();
    myCreate_machineWindow->show();
}

void MainWindow::on_run_machine_btn_clicked() {
    QDir machinesPath(QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines/");
    QString machine = ui->machine_listView->currentIndex().data().toString();
    parseconfig(machinesPath.absolutePath() + "/" + machine + ".json");
}

void MainWindow::on_modify_machine_btn_clicked() {
    QModelIndex ModifyMachineIndex = ui->machine_listView->currentIndex();
    int realIndex = ModifyMachineIndex.row();
    QString MachineToModifyName = model->index( realIndex, 0 ).data( Qt::DisplayRole ).toString();
    QString MachineToModify = QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines/" + MachineToModifyName + ".json";
    myModify_machineWindow = new modify_machine(MachineToModify);
    this->close();
    myModify_machineWindow->show();
}

void MainWindow::on_remove_machine_btn_clicked() {
    QModelIndex ModifyMachineIndex = ui->machine_listView->currentIndex();
    int realIndex = ModifyMachineIndex.row();
    QString MachineToModifyName = model->index( realIndex, 0 ).data( Qt::DisplayRole ).toString();
    QDir machinesPath(QString::fromUtf8(homedir) + "/.config/QuiEmu/");
    const QString folderPath = machinesPath.filePath("Machines/");

    QMessageBox::StandardButton ReallyRemove;
      ReallyRemove = QMessageBox::question(this, "Remove Machine", "Delete Machine " + MachineToModifyName + " ?", QMessageBox::Yes|QMessageBox::No);
      if (ReallyRemove == QMessageBox::Yes) {
          QModelIndex DeleteMachineIndex = ui->machine_listView->currentIndex();
          int realIndex = DeleteMachineIndex.row();
          QString MachineToRemoveName = model->index( realIndex, 0 ).data( Qt::DisplayRole ).toString();
          QString MachineToRemove = QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines/" + MachineToRemoveName + ".json";
          QFile MachineToRemoveFile (MachineToRemove);
          MachineToRemoveFile.remove();

          if(!folderPath.isEmpty()) {
              QDir machinesPath(QString::fromUtf8(homedir) + "/.config/QuiEmu/");
              const QString folderPath = machinesPath.filePath("Machines/");

              QDir dir(folderPath);
              QStringList filter;
              filter << QLatin1String("*.json");
              dir.setNameFilters(filter);
              QFileInfoList filelistinfo = dir.entryInfoList();
              machines.clear();
              foreach (const QFileInfo &fileinfo, filelistinfo) {
                  machines << fileinfo.fileName().remove(".json");
              }
              model->setStringList(machines);
              ui->machine_listView->setModel(model);
              ui->machine_listView->update();
              ui->machine_listView->setCurrentIndex(model->index(0,0));
              ui->machine_listView->clicked(model->index(0,0));
          }
      }
}

void MainWindow::listMachineDetails(int index) {
    QString MachineToListName = model->index( index, 0 ).data( Qt::DisplayRole ).toString();
    QString MachineToList = QString::fromUtf8(homedir) + "/.config/QuiEmu/Machines/" + MachineToListName + ".json";

    QString val;
    QFile file;
    file.setFileName(MachineToList);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();
    QJsonDocument MachineFile = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject MachineHandler = MachineFile.object();
    QJsonValue MachineName = MachineHandler.value(QString("MachineName"));
    QJsonValue SystemType = MachineHandler.value(QString("SystemType"));
    QJsonValue CDImage = MachineHandler.value(QString("CDImage"));
    QJsonValue DiskImage = MachineHandler.value(QString("DiskImage"));
    QJsonValue RamSize = MachineHandler["RamSize"];
    QJsonValue MachineType = MachineHandler.value(QString("MachineType"));
    QJsonValue EnableKVM = MachineHandler["EnableKVM"];
    QJsonValue CPUType = MachineHandler.value(QString("CPUType"));
    QJsonValue EnableSPICE = MachineHandler["EnableSPICE"];
    QJsonValue SpicePort = MachineHandler["SpicePort"];
    QJsonValue EnableGraphics = MachineHandler["EnableGraphics"];
    QJsonValue GraphicsCard = MachineHandler.value(QString("GraphicsCard"));
    QJsonValue EnableNetworking = MachineHandler["EnableNetworking"];
    QJsonValue NetworkInterface1 = MachineHandler.value(QString("NetworkInterface1"));
    QJsonValue NetworkInterface2 = MachineHandler.value(QString("NetworkInterface2"));
    QJsonValue NetworkInterface3 = MachineHandler.value(QString("NetworkInterface3"));

    QStandardItemModel *DetailsModel = new QStandardItemModel(this);
    DetailsModel->setColumnCount(2);
    DetailsModel->setRowCount(16);
    QStringList header;
    header << "Property Name" << "Property Value";
    DetailsModel->setHorizontalHeaderLabels(header);
    DetailsModel->setData(DetailsModel->index(0, 0), "Machine Name");
    DetailsModel->setData(DetailsModel->index(1, 0), "System Type");
    DetailsModel->setData(DetailsModel->index(2, 0), "Machine Type");
    DetailsModel->setData(DetailsModel->index(3, 0), "CPU Type");
    DetailsModel->setData(DetailsModel->index(4, 0), "CD Image");
    DetailsModel->setData(DetailsModel->index(5, 0), "Disk Image");
    DetailsModel->setData(DetailsModel->index(6, 0), "RAM Size");
    DetailsModel->setData(DetailsModel->index(7, 0), "KVM enabled");
    DetailsModel->setData(DetailsModel->index(8, 0), "Graphics enabled");
    DetailsModel->setData(DetailsModel->index(9, 0), "Graphics Card");
    DetailsModel->setData(DetailsModel->index(10, 0), "SPICE enabled");
    DetailsModel->setData(DetailsModel->index(11, 0), "SPICE Port");
    DetailsModel->setData(DetailsModel->index(12, 0), "Networking enabled");
    DetailsModel->setData(DetailsModel->index(13, 0), "Networkinterface 1");
    DetailsModel->setData(DetailsModel->index(14, 0), "Networkinterface 2");
    DetailsModel->setData(DetailsModel->index(15, 0), "Networkinterface 3");
    DetailsModel->setData(DetailsModel->index(0, 1), MachineName.toString());
    DetailsModel->setData(DetailsModel->index(1, 1), SystemType.toString());
    DetailsModel->setData(DetailsModel->index(2, 1), MachineType.toString());
    DetailsModel->setData(DetailsModel->index(3, 1), CPUType.toString());
    DetailsModel->setData(DetailsModel->index(4, 1), CDImage.toString());
    DetailsModel->setData(DetailsModel->index(5, 1), DiskImage.toString());
    DetailsModel->setData(DetailsModel->index(6, 1), RamSize.toString());
    DetailsModel->setData(DetailsModel->index(7, 1), EnableKVM.toBool());
    DetailsModel->setData(DetailsModel->index(8, 1), EnableGraphics.toBool());
    DetailsModel->setData(DetailsModel->index(9, 1), GraphicsCard.toString());
    DetailsModel->setData(DetailsModel->index(10, 1), EnableSPICE.toBool());
    DetailsModel->setData(DetailsModel->index(11, 1), SpicePort.toString());
    DetailsModel->setData(DetailsModel->index(12, 1), EnableNetworking.toBool());
    DetailsModel->setData(DetailsModel->index(13, 1), NetworkInterface1.toString());
    DetailsModel->setData(DetailsModel->index(14, 1), NetworkInterface2.toString());
    DetailsModel->setData(DetailsModel->index(15, 1), NetworkInterface3.toString());
    ui->machine_details_treeView->setModel(DetailsModel);
    ui->machine_details_treeView->resizeColumnToContents(0);
    ui->machine_details_treeView->resizeColumnToContents(1);
    ui->machine_details_treeView->update();
}

void MainWindow::on_machine_listView_clicked() {
    QModelIndex ModifyMachineIndex = ui->machine_listView->currentIndex();
    int realIndex = ModifyMachineIndex.row();
    listMachineDetails(realIndex);
}

void MainWindow::on_actionAppearance_triggered() {
    this->close();
    myAppearanceWindow = new appearance();
    myAppearanceWindow->show();
}
