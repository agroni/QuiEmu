#include "src/choose_cd_image.h"
#include "ui_choose_cd_image.h"
#include "src/mainwindow.h"

QString CD_FileType;
QString CD_DestinationFileName;

choose_cd_image::choose_cd_image(QWidget *parent) : QWidget(parent),
    ui(new Ui::choose_cd_image) {
        ui->setupUi(this);
}

choose_cd_image::~choose_cd_image() {
    delete ui;
}

void choose_cd_image::on_cancel_btn_clicked() {
    this->hide();
    MainWindow *myMainWindow = new MainWindow();
    myMainWindow->show();
}

void choose_cd_image::on_previous_btn_clicked() {
    this->hide();
    create_machine2* myCreate_machine2Window = new create_machine2();
    myCreate_machine2Window->window()->show();
}

void choose_cd_image::on_next_btn_clicked() {
    if(ui->yes_radio_btn->isChecked()) {
        if(!ui->cd_image_path_txt->text().isEmpty()) {
            this->hide();
            create_machine3 *myCreate_machine3Window = new create_machine3();
            myCreate_machine3Window->show();
        } else {
            QErrorMessage EmptyCDError(this);
            EmptyCDError.showMessage("Please specifiy a CD-Image");
            EmptyCDError.exec();
          }
    }
    if(ui->no_radio_btn->isChecked()) {
        CD_DestinationFileName = "";
        this->hide();
        create_machine3 *myCreate_machine3Window = new create_machine3();
        myCreate_machine3Window->show();
    }
}

void choose_cd_image::on_choose_cdrom_btn_clicked() {
    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::ExistingFile);
    CD_DestinationFileName = dialog.getOpenFileName(this, tr("Select File"), QDir::currentPath(), tr("CD-ROM Images (*.iso *.img *.dmg)"));
    ui->cd_image_path_txt->setText(CD_DestinationFileName);
}

void choose_cd_image::on_yes_radio_btn_clicked() {
    ui->cd_image_path_txt->setEnabled(true);
    ui->choose_cdrom_btn->setEnabled(true);
}

void choose_cd_image::on_no_radio_btn_clicked() {
    ui->cd_image_path_txt->setEnabled(false);
    ui->choose_cdrom_btn->setEnabled(false);
    CD_DestinationFileName = "";
}
