#include "src/mainwindow.h"
#include "src/create_diskimage.h"
#include "ui_create_diskimage.h"

QString DestinationFileName;
QString FileSize;
QStringList arguments;
QString FileType;

create_diskimage::create_diskimage(QWidget *parent) : QWidget(parent), ui(new Ui::create_diskimage) {
        ui->setupUi(this);
}

create_diskimage::~create_diskimage() {
    delete ui;
}

void create_diskimage::on_choose_location_btn_clicked() {

    QFileDialog dialog;
    dialog.setFileMode(QFileDialog::AnyFile);

    switch(ui->diskImageType_ComboBox->currentIndex()) {
        case 0:
        // User selected qcow2 file extension
        FileType = "qcow2";
        DestinationFileName = dialog.getSaveFileName(NULL, "Create New File","",FileType);

        break;
        case 1:
        // User selected vdi file extension
        FileType = "vdi";
        DestinationFileName = dialog.getSaveFileName(NULL, "Create New File","",FileType);

        break;
    }
}

void create_diskimage::on_create_image_btn_clicked() {
    if(ui->diskImageType_ComboBox->currentIndex()==0) {
            // User selected qcow2 file extension
            FileSize = ui->size_txt->text();
            DestinationFileName+="."+FileType;
            FileSize+="G";

            QString program = "qemu-img";
            QStringList arguments;
            arguments << "create" << "-f" << FileType << DestinationFileName << FileSize;
            QProcess *DiskImageCreator = new QProcess(this);
            DiskImageCreator->start(program, arguments);

            QMessageBox msgBox;
            msgBox.setText("Diskimage of Type " + FileType + " with size of " + FileSize + " at " + DestinationFileName + " has been successfully created!");
            msgBox.exec();
    }
    if(ui->diskImageType_ComboBox->currentIndex()==1) {
            // User selected vdi file extension
            FileSize = ui->size_txt->text();
            DestinationFileName+="."+FileType;

            QString program = "VBoxManage";
            QStringList arguments;
            arguments << "createhd" << "-filename" << DestinationFileName << "-size" << FileSize;
            QProcess *DiskImageCreator = new QProcess(this);
            DiskImageCreator->start(program, arguments);

            QMessageBox msgBox;
            msgBox.setText("Diskimage of Type " + FileType + " with size of " + FileSize + "G at " + DestinationFileName + " has been successfully created!");
            msgBox.exec();
    }
    this->hide();
}
