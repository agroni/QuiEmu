#include "src/run_machine.h"
#include "src/parseconfig.h"
#include "QDebug"
#include <iostream>

parseconfig::parseconfig(QString configFile) {
    QString val;
    QFile file;
    file.setFileName(configFile);
    file.open(QIODevice::ReadOnly | QIODevice::Text);
    val = file.readAll();
    file.close();
    QJsonDocument MachineFile = QJsonDocument::fromJson(val.toUtf8());
    QJsonObject MachineHandler = MachineFile.object();
    QJsonValue MachineName = MachineHandler.value(QString("MachineName"));
    QJsonValue SystemType = MachineHandler.value(QString("SystemType"));
    QJsonValue CDImage = MachineHandler.value(QString("CDImage"));
    QJsonValue DiskImage = MachineHandler.value(QString("DiskImage"));
    QJsonValue RamSize = MachineHandler["RamSize"];
    QJsonValue MachineType = MachineHandler.value(QString("MachineType"));
    QJsonValue EnableKVM = MachineHandler["EnableKVM"];
    QJsonValue CPUType = MachineHandler.value(QString("CPUType"));
    QJsonValue EnableSPICE = MachineHandler["EnableSPICE"];
    QJsonValue SpicePort = MachineHandler["SpicePort"];
    QJsonValue EnableGraphics = MachineHandler["EnableGraphics"];
    QJsonValue GraphicsCard = MachineHandler.value(QString("GraphicsCard"));
    QJsonValue EnableNetworking = MachineHandler["EnableNetworking"];
    QJsonValue BypassHostInterface = MachineHandler["BypassHostInterface"];
    QJsonValue NetworkInterface1 = MachineHandler.value(QString("NetworkInterface1"));
    QJsonValue NetworkInterface2 = MachineHandler.value(QString("NetworkInterface2"));
    QJsonValue NetworkInterface3 = MachineHandler.value(QString("NetworkInterface3"));

    if(CDImage.toString()!=NULL) {
        cd_image_set = true;
    } else {
        cd_image_set = false;
    }
    if(DiskImage.toString()!=NULL) {
        disk_image_set = true;
    } else {
        disk_image_set = false;
    }
    if(CPUType.toString()!=NULL) {
        CPUType_set = true;
    } else {
        CPUType_set = false;
    }

    run_machine(MachineName.toString(), SystemType.toString(), cd_image_set, CDImage.toString(), disk_image_set, DiskImage.toString(), RamSize.toString(), MachineType.toString(), EnableKVM.toBool(), CPUType_set, CPUType.toString(), EnableSPICE.toBool(), SpicePort.toString(), EnableGraphics.toBool(), GraphicsCard.toString(), EnableNetworking.toBool(), BypassHostInterface.toBool(), NetworkInterface1.toString(), NetworkInterface2.toString(), NetworkInterface3.toString());
}
