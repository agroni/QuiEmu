#ifndef RUN_MACHINE_H
#define RUN_MACHINE_H

#include <QString>
#include <QStringList>
#include <QProcess>
#include <QEventLoop>
#include <QFile>
#include <QErrorMessage>

class run_machine
{
public:
    //run_machine(QString MachineName, QString SystemType, QString CD_DestinationFileName , QString DiskImage, QString ramSize, QString MachineType, bool enable_kvm, QString CPUType, bool enable_spice, QString spice_port, bool enable_graphics, QString GraphicsCard);
    run_machine(QString MachineName, QString SystemType, bool cd_image_set, QString CDImage , bool disk_image_set, QString DiskImage, QString RamSize, QString MachineType, bool enable_kvm, bool CPUType_set, QString CPUType, bool enable_spice, QString spice_port, bool enable_graphics, QString GraphicsCard, bool enable_networking,bool bypass_host_interface, QString NetworkInterface1, QString NetworkInterface2, QString NetworkInterface3);
    ~run_machine();
    QString program;
    QStringList arguments;
    QStringList SpicyArguments;
    QProcess *MachineRunner;
    QProcess *SpicyRunner;
    QErrorMessage DiskImageError;
};

#endif // RUN_MACHINE_H
