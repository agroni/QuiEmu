#ifndef MODIFY_MACHINE_H
#define MODIFY_MACHINE_H

#include <QWidget>
#include <QString>
#include <QFile>
#include <QJsonDocument>
#include <QJsonObject>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "writeconfig.h"
#include "create_diskimage.h"

namespace Ui {
class modify_machine;
}

class modify_machine : public QWidget
{
    Q_OBJECT

public:
    explicit modify_machine(QString MachineToModify, QWidget *parent = 0);
    ~modify_machine();

private slots:
    void on_apply_btn_clicked();

    void on_ram_slider_valueChanged(int value);

    void on_ram_size_txt_textChanged(const QString &ramSize2);

    void on_create_new_diskimage_radio_clicked();

    void on_use_existing_image_radio_clicked();

    void on_image_choose_btn_clicked();

    void on_choose_cdrom_btn_clicked();

    void on_enable_cd_checkbox_toggled(bool checked);

    void on_enable_graphics_checkbox_toggled(bool checked);

    void on_enable_spice_checkbox_toggled(bool checked);

    void on_exit_btn_clicked();

    void on_enable_Networking_checkBox_toggled(bool checked);

    void on_enable_if1_checkBox_toggled(bool checked);

    void on_enable_if2_checkBox_toggled(bool checked);

    void on_enable_if3_checkBox_toggled(bool checked);

    void on_enable_samba_share_checkBox_toggled(bool checked);

private:
    Ui::modify_machine *ui;
    QString val2;
    QFile file2;
    QJsonDocument MachineFile2;
    QJsonObject MachineHandler2;
    QJsonValue MachineName2;
    QJsonValue SystemType2;
    QJsonValue CDImage2;
    QJsonValue DiskImage2;
    QJsonValue ramSize2;
    QJsonValue MachineType;
    QJsonValue CPU_Type;
    QJsonValue GraphicsCard;
    QJsonValue SpicePort;
    QJsonValue SambaSharePath;
    QJsonValue NetworkInterface1;
    QJsonValue NetworkInterface2;
    QJsonValue NetworkInterface3;
    QString MachineToModify;
    QString CD_FileType2;
    QString CD_DestinationFileName2;
    QString Graphics_Card;
    bool enable_spice;
    bool enable_kvm;
    bool enable_graphics;
    int spice_port;
    bool enable_networking;
    bool bypass_host_interface;
    bool enable_if1;
    bool enable_if2;
    bool enable_if3;
    bool enable_samba;
    create_diskimage *myCreate_diskimageWindow2;
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
};

#endif // MODIFY_MACHINE_H
