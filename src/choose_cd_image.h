#ifndef CHOOSE_CD_IMAGE_H
#define CHOOSE_CD_IMAGE_H

#include <QWidget>
#include "create_machine3.h"

namespace Ui {
class choose_cd_image;
}

class choose_cd_image : public QWidget
{
    Q_OBJECT

public:
    explicit choose_cd_image(QWidget *parent = 0);
    ~choose_cd_image();
    create_machine3 *myCreate_machine3Window;

private slots:
    void on_cancel_btn_clicked();

    void on_previous_btn_clicked();

    void on_next_btn_clicked();

    void on_choose_cdrom_btn_clicked();

    void on_yes_radio_btn_clicked();

    void on_no_radio_btn_clicked();

private:
    Ui::choose_cd_image *ui;

};

#endif // CHOOSE_CD_IMAGE_H
