#include "src/create_machine.h"
#include "src/create_machine2.h"
#include "src/create_machine3.h"
#include "ui_create_machine.h"
#include "src/mainwindow.h"

QString MachineName;
QString SystemType;

create_machine::create_machine(QWidget *parent) : QWidget(parent), ui(new Ui::create_machine) {
        ui->setupUi(this);
}

create_machine::~create_machine() {
    delete ui;
}

void create_machine::on_next_btn_clicked() {

    if(!ui->machine_name_text->text().isEmpty()) {
        MachineName = ui->machine_name_text->text();
        if(ui->machine_Type_comboBox->currentIndex()==0) {
            SystemType = "Linux";
        }
        if(ui->machine_Type_comboBox->currentIndex()==1) {
            SystemType = "BSD";
        }
        if(ui->machine_Type_comboBox->currentIndex()==2) {
            SystemType = "OSX";
        }
        if(ui->machine_Type_comboBox->currentIndex()==3) {
            SystemType = "Solaris";
        }
        if(ui->machine_Type_comboBox->currentIndex()==4) {
            SystemType = "Windows";
        }
        myCreate_machine2Window = new create_machine2();
        myCreate_machine2Window->show();
        this->hide();
    } else {
        QErrorMessage NameError(this);
        NameError.showMessage("Please specifiy a Machine Name");
        NameError.exec();
    }
}

void create_machine::on_cancel_btn_clicked() {
    this->close();
    MainWindow *myMainWindow = new MainWindow();
    myMainWindow->show();
}
