/********************************************************************************
** Form generated from reading UI file 'appearance.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_APPEARANCE_H
#define UI_APPEARANCE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_appearance
{
public:
    QWidget *gridLayoutWidget;
    QGridLayout *gridLayout;
    QLabel *label;
    QComboBox *theme_comboBox;
    QWidget *horizontalLayoutWidget;
    QHBoxLayout *horizontalLayout;
    QPushButton *cancel_btn;
    QPushButton *apply_btn;

    void setupUi(QWidget *appearance)
    {
        if (appearance->objectName().isEmpty())
            appearance->setObjectName(QStringLiteral("appearance"));
        appearance->resize(640, 480);
        gridLayoutWidget = new QWidget(appearance);
        gridLayoutWidget->setObjectName(QStringLiteral("gridLayoutWidget"));
        gridLayoutWidget->setGeometry(QRect(60, 50, 511, 80));
        gridLayout = new QGridLayout(gridLayoutWidget);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        gridLayout->setContentsMargins(100, 0, 100, 0);
        label = new QLabel(gridLayoutWidget);
        label->setObjectName(QStringLiteral("label"));

        gridLayout->addWidget(label, 1, 0, 1, 1);

        theme_comboBox = new QComboBox(gridLayoutWidget);
        theme_comboBox->setObjectName(QStringLiteral("theme_comboBox"));

        gridLayout->addWidget(theme_comboBox, 1, 1, 1, 1);

        horizontalLayoutWidget = new QWidget(appearance);
        horizontalLayoutWidget->setObjectName(QStringLiteral("horizontalLayoutWidget"));
        horizontalLayoutWidget->setGeometry(QRect(300, 380, 321, 81));
        horizontalLayout = new QHBoxLayout(horizontalLayoutWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        cancel_btn = new QPushButton(horizontalLayoutWidget);
        cancel_btn->setObjectName(QStringLiteral("cancel_btn"));

        horizontalLayout->addWidget(cancel_btn);

        apply_btn = new QPushButton(horizontalLayoutWidget);
        apply_btn->setObjectName(QStringLiteral("apply_btn"));

        horizontalLayout->addWidget(apply_btn);


        retranslateUi(appearance);

        QMetaObject::connectSlotsByName(appearance);
    } // setupUi

    void retranslateUi(QWidget *appearance)
    {
        appearance->setWindowTitle(QApplication::translate("appearance", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("appearance", "Theme", Q_NULLPTR));
        theme_comboBox->clear();
        theme_comboBox->insertItems(0, QStringList()
         << QApplication::translate("appearance", "Light (Default)", Q_NULLPTR)
         << QApplication::translate("appearance", "Dark", Q_NULLPTR)
        );
        cancel_btn->setText(QApplication::translate("appearance", "Cancel", Q_NULLPTR));
        apply_btn->setText(QApplication::translate("appearance", "Apply", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class appearance: public Ui_appearance {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_APPEARANCE_H
