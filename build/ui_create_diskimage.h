/********************************************************************************
** Form generated from reading UI file 'create_diskimage.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATE_DISKIMAGE_H
#define UI_CREATE_DISKIMAGE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_create_diskimage
{
public:
    QGroupBox *groupBox;
    QLabel *label;
    QComboBox *diskImageType_ComboBox;
    QPushButton *choose_location_btn;
    QPushButton *create_image_btn;
    QLabel *label_2;
    QLineEdit *size_txt;
    QLabel *label_3;

    void setupUi(QWidget *create_diskimage)
    {
        if (create_diskimage->objectName().isEmpty())
            create_diskimage->setObjectName(QStringLiteral("create_diskimage"));
        create_diskimage->resize(481, 331);
        groupBox = new QGroupBox(create_diskimage);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        groupBox->setGeometry(QRect(0, 0, 481, 331));
        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(130, 100, 121, 16));
        QFont font;
        font.setFamily(QStringLiteral("Open Sans"));
        font.setPointSize(10);
        font.setBold(false);
        font.setItalic(false);
        font.setWeight(50);
        label->setFont(font);
        diskImageType_ComboBox = new QComboBox(groupBox);
        diskImageType_ComboBox->setObjectName(QStringLiteral("diskImageType_ComboBox"));
        diskImageType_ComboBox->setGeometry(QRect(260, 95, 81, 22));
        QFont font1;
        font1.setFamily(QStringLiteral("Open Sans"));
        font1.setPointSize(10);
        diskImageType_ComboBox->setFont(font1);
        choose_location_btn = new QPushButton(groupBox);
        choose_location_btn->setObjectName(QStringLiteral("choose_location_btn"));
        choose_location_btn->setGeometry(QRect(130, 170, 161, 22));
        choose_location_btn->setFont(font1);
        create_image_btn = new QPushButton(groupBox);
        create_image_btn->setObjectName(QStringLiteral("create_image_btn"));
        create_image_btn->setGeometry(QRect(130, 200, 131, 22));
        create_image_btn->setFont(font1);
        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(220, 130, 31, 16));
        label_2->setFont(font1);
        size_txt = new QLineEdit(groupBox);
        size_txt->setObjectName(QStringLiteral("size_txt"));
        size_txt->setGeometry(QRect(260, 130, 81, 22));
        size_txt->setFont(font1);
        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(350, 130, 21, 16));
        label_3->setFont(font1);

        retranslateUi(create_diskimage);

        QMetaObject::connectSlotsByName(create_diskimage);
    } // setupUi

    void retranslateUi(QWidget *create_diskimage)
    {
        create_diskimage->setWindowTitle(QApplication::translate("create_diskimage", "Form", Q_NULLPTR));
        groupBox->setTitle(QApplication::translate("create_diskimage", "Create New Diskimage", Q_NULLPTR));
        label->setText(QApplication::translate("create_diskimage", "Type of Disk Image", Q_NULLPTR));
        diskImageType_ComboBox->clear();
        diskImageType_ComboBox->insertItems(0, QStringList()
         << QApplication::translate("create_diskimage", "qcow2", Q_NULLPTR)
         << QApplication::translate("create_diskimage", "vdi", Q_NULLPTR)
        );
        choose_location_btn->setText(QApplication::translate("create_diskimage", "Choose Location", Q_NULLPTR));
        create_image_btn->setText(QApplication::translate("create_diskimage", "Create Image", Q_NULLPTR));
        label_2->setText(QApplication::translate("create_diskimage", "Size", Q_NULLPTR));
        label_3->setText(QApplication::translate("create_diskimage", "GB", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class create_diskimage: public Ui_create_diskimage {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATE_DISKIMAGE_H
