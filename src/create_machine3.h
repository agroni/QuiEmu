#ifndef CREATE_MACHINE3_H
#define CREATE_MACHINE3_H

#include <QWidget>
#include <QString>
#include <unistd.h>
#include <sys/types.h>
#include <pwd.h>
#include "create_diskimage.h"

namespace Ui {
class create_machine3;
}

class create_machine3 : public QWidget
{
    Q_OBJECT

public:
    explicit create_machine3(QWidget *parent = 0);
    ~create_machine3();

private slots:

    void on_file_choose_btn_clicked();

    void on_radio0_clicked();

    void on_radio1_clicked();

    void on_finish_next_btn_clicked();

    void on_previous_btn_clicked();

private:
    Ui::create_machine3 *ui;
    create_diskimage *myCreate_diskimageWindow;
    struct passwd *pw = getpwuid(getuid());
    const char *homedir = pw->pw_dir;
    QString DiskImage;
    QString FileType2;
    QString configFile;
};

#endif // CREATE_MACHINE3_H
