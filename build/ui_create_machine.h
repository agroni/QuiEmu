/********************************************************************************
** Form generated from reading UI file 'create_machine.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_CREATE_MACHINE_H
#define UI_CREATE_MACHINE_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_create_machine
{
public:
    QLabel *label;
    QLabel *label_2;
    QLineEdit *machine_name_text;
    QLabel *label_3;
    QPushButton *cancel_btn;
    QPushButton *next_btn;
    QComboBox *machine_Type_comboBox;

    void setupUi(QWidget *create_machine)
    {
        if (create_machine->objectName().isEmpty())
            create_machine->setObjectName(QStringLiteral("create_machine"));
        create_machine->resize(802, 460);
        label = new QLabel(create_machine);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(270, 50, 261, 61));
        QFont font;
        font.setFamily(QStringLiteral("Open Sans"));
        font.setPointSize(14);
        label->setFont(font);
        label_2 = new QLabel(create_machine);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(240, 160, 51, 16));
        QFont font1;
        font1.setFamily(QStringLiteral("Open Sans"));
        font1.setPointSize(11);
        font1.setBold(false);
        font1.setItalic(false);
        font1.setWeight(50);
        label_2->setFont(font1);
        machine_name_text = new QLineEdit(create_machine);
        machine_name_text->setObjectName(QStringLiteral("machine_name_text"));
        machine_name_text->setGeometry(QRect(300, 160, 221, 22));
        label_3 = new QLabel(create_machine);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(240, 210, 59, 21));
        label_3->setFont(font1);
        cancel_btn = new QPushButton(create_machine);
        cancel_btn->setObjectName(QStringLiteral("cancel_btn"));
        cancel_btn->setGeometry(QRect(240, 280, 81, 22));
        next_btn = new QPushButton(create_machine);
        next_btn->setObjectName(QStringLiteral("next_btn"));
        next_btn->setGeometry(QRect(440, 280, 81, 22));
        next_btn->setStyleSheet(QStringLiteral("QPushButton:focus { outline: 0 }"));
        next_btn->setAutoDefault(false);
        machine_Type_comboBox = new QComboBox(create_machine);
        machine_Type_comboBox->setObjectName(QStringLiteral("machine_Type_comboBox"));
        machine_Type_comboBox->setGeometry(QRect(300, 210, 221, 22));

        retranslateUi(create_machine);

        next_btn->setDefault(false);


        QMetaObject::connectSlotsByName(create_machine);
    } // setupUi

    void retranslateUi(QWidget *create_machine)
    {
        create_machine->setWindowTitle(QApplication::translate("create_machine", "Form", Q_NULLPTR));
        label->setText(QApplication::translate("create_machine", "Name and Operating System", Q_NULLPTR));
        label_2->setText(QApplication::translate("create_machine", "Name", Q_NULLPTR));
        label_3->setText(QApplication::translate("create_machine", "Type", Q_NULLPTR));
        cancel_btn->setText(QApplication::translate("create_machine", "Cancel", Q_NULLPTR));
        next_btn->setText(QApplication::translate("create_machine", "Next", Q_NULLPTR));
        machine_Type_comboBox->clear();
        machine_Type_comboBox->insertItems(0, QStringList()
         << QApplication::translate("create_machine", "Linux", Q_NULLPTR)
         << QApplication::translate("create_machine", "BSD", Q_NULLPTR)
         << QApplication::translate("create_machine", "Mac OSX", Q_NULLPTR)
         << QApplication::translate("create_machine", "Solaris", Q_NULLPTR)
         << QApplication::translate("create_machine", "Windows", Q_NULLPTR)
        );
    } // retranslateUi

};

namespace Ui {
    class create_machine: public Ui_create_machine {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_CREATE_MACHINE_H
